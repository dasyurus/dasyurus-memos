# Dasyurus Memos

| Metadata |                                                   |
|----------|---------------------------------------------------|
| Memo     | 1                                                 |
| Version  | 1.0                                               |
| Title    | Dasyurus Memos                                    |
| Authors  | Muhammad Kaisar Arkhan (<mailto:hi@yukiisbo.red>) |
| Status   | Active                                            |
| Type     | Process                                           |

## Abstract

This memo describes the introduction of memos to the Dasyurus community.

Memos are inspired by [coala's cEPs][cEPs] and [Python project's PEP1][PEP1].

## What is a memo?

A memo is a document which provides information to the community or describing a
major change to the project or its community.

## Purpose

Memos are used as a way to propose major changes and discuss them with the
community.

In order for a memo to be accepted, the memo author is responsible for building
consensus within the community and document dissenting opinions.

After a memo is accepted by the community, the proposed changes should be
implemented as described in the memo.

## Spelling

Memo is spelled as it is used in the English language. The filename of a memo
must follow `MEMO-[0-9]+.md`.

Within the community, a memo may be referred to as `MEMO-[0-9]+`. However, it is
acceptable to refer it naturally (such as "Dasyurus Memo no. 1", "Memo number
1").

## Basic requirements

The following is a list of basic requirements that every memo must follow in
order to be accepted:

1. Memos must answer what the change is and why the change is required.
2. Memos must be complete when describing the changes.
3. Memos must be written in the [GitLab Markdown][glm] markup language.
4. Memos must follow the structure as described below.

## Structure

Memos must at least contain the following information in order:

1. Title
2. Metadata
3. Abstract

Further chapters may be added as needed.

### Title

Memo titles should be descriptive but brief. It does not need to explain the
whole memo but only the basic idea underlying it without being a paragraph.

### Metadata

Metadata within memos must be a [CommonMark table][md-table] which represents
data in a [key-value][kv-wiki] format.

The header of the metadata table must only be "Metadata" on the first column
with blank on the second column.

Metadata must contain the following information:

1. A **Memo number.** This number is final and assigned when the memo is
   accepted.
2. A **Version number.** This version number starts with 0.1 and raise until
   they get accepted, which bumps the version to 1.0. They must follow [semantic
   versioning][semver].
3. A **Title**. This title must follow the requirements described above.
4. The name(s) and e-mail address(es) of the **Authors**. They must follow the
   following format: `Your name here (<mailto:youremail@address.tld>)`. If there
   are multiple authors, they must be separated by commas.
5. A **Status** which must follow one of the possible statuses described below.
6. A **Type** which must follow one of the possible types described below.

### Abstract

Abstract is a short description which describes the basic proposed idea. Unlike
a title, an abstract must be at least a paragraph.

## Status

Memos must be in one of the following statuses.

### Status: Proposed

Memos can be proposed by any contributor. In order to propose a memo, a merge
request to the [memo repository][memo-repo] must be made. The merge request is
reviewed like any merge requests but must enter with a `Proposed` state.

If a memo is not merged, it is invalid and has no state.

All discussion on the memo should be done in the merge request.

Follow up states are `Implementation Due`, `Active` or `Rejected`.

### Status: Implementation Due

Feature requests may be set to this state if they have been discussed and
accepted but are not implemented yet.

Follow up states are `Active` or `Rejected`.

### Status: Active

Memos may be active state when the proposed changes are merged into the project
and is being actively maintained.

Follow up states are `Deprecated`.

### Status: Rejected

Memos may be rejected if the community determines.

Follow up states are `Proposed`.

### Status: Deprecated

Memos may be marked deprecated if its contents are no longer useful to the
community. If a follow up memo exists, it should be cited within the original
memo.

## Type

Memos must be in one of the following types.

### Type: Standard

Standard memos are memos which proposes technical changes for the project
itself. It describes a new feature or implementation for the project.

### Type: Process

Process memos are memos which proposes changes that directly involve the
community itself. They are like standard memos but involves areas other than the
project itself. Examples includes procedures, guidelines, changes to the
decision-making process. Any meta-memo is also considered a process memo.

## The lifecycle of a memo

1. A memo is issued by making a merge request to the [memo
   repository][memo-repo]. This can be started by anyone within the community.
2. Discussion starts in the merge request itself. The community may discuss this
   in other areas but officially, only the ones in the merge request are
   considered. Anyone in the community may express whether they accept or refuse
   the change along with their opinions.
3. Any refusal from the community must have their opinions addressed by the
   original author. It is the author job to address these opinions and ensure a
   general consensus.
4. Changes can be made by the author until the memo is accepted or until a
   general consensus is reached.
5. When a general consensus is reached, the proposed memo is accepted into the
   memo repository in the `Implementation Due` state and changes must be
   applied.
6. When all of the proposed changes from the memo is made, the memo will enter
   to the `Active` state.
7. However, if the changes are proven to be unfeasible or the community reached
   a general consensus to reject the memo, it can enter the `Reject` state and
   may be able to be reproposed later.
8. In the future, when the original changes of the memo no longer reflect the
   current project, it can be marked as `Deprecated` if needed.

## Patching memos

Memos may be changed. If a memo is changed, the version number has to be
increased according to semantic versioning.

[PEP1]: https://www.python.org/dev/peps/pep-0001/
[cEPs]: https://github.com/coala/cEPs
[glm]: https://docs.gitlab.com/ee/user/markdown.html
[cm-spec]: https://spec.commonmark.org/current/
[kv-wiki]: https://en.wikipedia.org/wiki/Key-value_database
[semver]: https://semver.org/
[memo-repo]: https://gitlab.com/dasyurus/dasyurus-memos
